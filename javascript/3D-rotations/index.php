<!doctype html>

<!--
  This is a simple example / template impress.js slide show. The goal is to be
  easier to read for a first timer than the official and very feature rich
  demo by bartaz (http://bartaz.github.io/impress.js/). It's also a very
  traditional presentation that looks like slides (square screens with bullet
  points...), again to make a first timer feel more at home. From this simple
  presentation you can then go on to more powerful impress.js presentations!
  
  This example is hopefully helpful for people that want to create both
  simple and (eventually) awesome presentations in impress.js and comfortable
  doing that directly in HTML.
  
  By: @henrikingo (Still based on the HTML from bartaz' demo.)  
    
-->

<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Anniversaire</title>
    <link href="css/bootstrap.css" rel="stylesheet" />
    <meta name="description" content="Explore impress.js in 3D" />
    <meta name="author" content="Henrik Ingo" />
    <link href="css/3D-rotations.css" rel="stylesheet" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@1.5.4/src/loadingoverlay.min.js"></script>
    <script type="text/javascript" src="js/Audio-HTML5.js"></script>
</head>
<body class="impress-not-supported">
    <script type="text/javascript">
        $.LoadingOverlay("show");
      $(window).load(function() {
         $.LoadingOverlay("hide");
      });
      var isMobile = false; //initiate as false
    // device detection
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
      || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
    if(isMobile){
      document.location.href='mobile.php';
    }
  </script>
<div class="fallback-message">
    <p>Your browser <b>doesn't support the features required</b> by impress.js, so you are presented with a simplified version of this presentation.</p>
    <p>For the best experience please use the latest <b>Chrome</b>, <b>Safari</b> or <b>Firefox</b> browser.</p>
</div>

<div id="impress" data-transition-duration="2000">
    <div id="overview" class="step overview" data-x="1350" data-y="100" data-z="100" data-scale="3" data-rotate-y="90">
    <img src="images/darl.gif">
    </div>
    <div id="overview2" class="step overview" data-x="2018" data-y="106" data-z="3018" data-scale="2">
    <img src="images/br.gif">
         <p>Depuis quelques jours,<br>
          Je n’arrête pas de penser à la meilleure façon de te souhaiter ton anniversaire. 
          J’ai tout d’abord pensé à un lâcher de pétales de rose depuis une montgolfière, 
          puis à écrire ton nom dans le ciel avec la fumée d’un avion, 
          à te faire livrer ton poids en chocolat ou même à faire venir 
          une fanfare pour chanter une chanson rien que pour toi. 
          <br>et Puis je me suis dis,
          pourquoi ne pas lui offrir une attention des plus précieuses : 
          un diaporama de photos codé en HTML et en Javascript rien que pour elle avec tout mon amour.
          J’espère qu’elle te plait ! Heureux anniversaire chérie. :*
         </p>
    </div>
    <div id="step-1" class="step" data-x="0" data-y="0" data-z="0"
         data-goto-prev="step-8">
          <p>Depuis le jour ou nous nous sommes rencontrés <br>
          Mon amour n'a fait qu'augmentée<br>
          Cela fait déjà plus d'un ans<br>
          Mais mon amour est encore plus grand</p>
    </div>

    <div id="step-2" class="step" data-x="420" data-y="-70" data-z="-250" data-rotate-z="45" data-rotate-y="-45" data-rotate-order="zyx">
        <p>Je ne peux même pas<br>
          Imaginer la vie sans toi<br>
          Nos épreuves nous ont rapprochés<br>
          Contre vents et marée</p>
    </div>

    <div id="step-3" class="step" data-x="700" data-y="350" data-z="-350" data-rotate-z="90" data-rotate-y="-90" data-rotate-order="zyx">
        <p>Nous sommes restés unis<br>
        Malgré tous les soucis<br>
        Avec nos bons et nos mauvais souvenirs<br>
        Tous nos pleurs et nos fous rire</p>
    </div>

    <div id="step-4" class="step" data-x="422" data-y="780" data-z="-250" data-rotate-z="135" data-rotate-y="-135" data-rotate-order="zyx">
        <p>Tu as fait de moi un homme comblée<br>
        Jamais je ne cesserais de t'aimer<br>
        Tu es mon amour<br>
        Pour toujours<br>
        Tu es ma moitié </p>
    </div>

    <div id="step-5" class="step" data-x="0" data-y="702" data-z="0" data-rotate-z="180" data-rotate-y="-180" data-rotate-order="zyx">
        <p>Je ne cesse de t'aimer depuis tout ce temps<br>
        Et de penser à toi chaque jours à tout moments<br>
        Ton amour m'est indispensable<br>
        Et ta présence irremplaçable</p>
    </div>

    <div id="step-6" class="step" data-x="379" data-y="780" data-z="270" data-rotate-z="135" data-rotate-y="-225" data-rotate-order="zyx">
        <p>Comme je te le dis souvent<br>
        Et je ne m'en lasse pas honnêtement<br>
        Tu es la personne au monde qui m'est le cher<br>
        Et c'est pour ça que je t'aimerai ma vie entière</p>
    </div>

    <div id="step-7" class="step" data-x="700" data-y="350" data-z="350" data-rotate-z="90" data-rotate-y="-270" data-rotate-order="zyx">
        <p>Partout ou je me trouve<br>
        Tu es présente dans mon esprit<br>
        Et toutes les choses que j'éprouve<br>
        Ne sont que pour toi ma chérie</p>
    </div>

    <div id="step-8" class="step" data-x="379" data-y="-70" data-z="270" data-rotate-z="45" data-rotate-y="-315" data-rotate-order="zyx">
        <p>Ma chérie, je n'utilise pas souvent ce mot et pourtant<br>
        Je chéris chaque moment passé près de toi.<br>
        Aujourd'hui, c'est ton anniversaire et pourtant<br>
        c'est moi qui ai l'impression d'avoir le cadeau<br>
        Celui de ta présence à mes cotés</p>
    </div>
    <div id="step-9" class="step" data-x="700" data-y="780" data-z="350" data-rotate-z="0" data-rotate-y="-355" data-rotate-order="zyx"
         data-goto-next="step-1">
        <p>Il y a toujours au fond de tes yeux<br>
		ce sourire qui me fait tellement de bien.<br>
		Ta main a encore la même douceur.<br>
		Les années passent, elles laissent des traces,<br>
		mais tant qu'elles les laisseront sur nous deux ensembles<br>
		Je ne peux espérer rien de mieux.</p>
    </div>
</div>

<div id="impress-toolbar"></div>
<div id="impress-help"></div>
<script type="text/javascript" src="js/impress.js"></script>
<script>
impress().init();
<?php 
  $color = ['#c32a8d','#5b5d5e','#34963b','#e15f00','#b61c6f','#b7050e','#ffffff','#e5d300','#a1cfcf'];
  for ($x = 1; $x <= 9; $x++) {
    echo "$('#step-".$x."').on('impress:stepenter', function(){ 
      $(this).find('p').css('color', '".$color[$x-1]."'); 
      $('body').animate({color: '".$color[$x-1]."', backgroundColor: '".$color[$x-1]."'}, 1000 );
    });";
  } 
 ?>
</script>
</body>
</html>
