//=========================================================================
// INIT GO JS FOR ENTITY RELATIONSHIP DIAGRAM
//=========================================================================
// this is an array used to create mode of the entity RELATIONSHIP graph
function initEntityRelationship(dataModelArray, linkModelArray){
   if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
   var $ = go.GraphObject.make;  // for conciseness in defining templates
   myDiagram =
     $(go.Diagram, "myDiagramDiv",  // must name or refer to the DIV HTML element
       {
         initialContentAlignment: go.Spot.Center,
         allowDelete: false,
         allowCopy: false,
         layout: $(go.ForceDirectedLayout),
         "undoManager.isEnabled": true
       });

   // define several shared Brushes
   var bluegrad = $(go.Brush, "Linear", { 0: "rgb(150, 150, 250)", 0.5: "rgb(86, 86, 186)", 1: "rgb(86, 86, 186)" });
   var greengrad = $(go.Brush, "Linear", { 0: "rgb(158, 209, 159)", 1: "rgb(67, 101, 56)" });
   var redgrad = $(go.Brush, "Linear", { 0: "rgb(206, 106, 100)", 1: "rgb(180, 56, 50)" });
   var yellowgrad = $(go.Brush, "Linear", { 0: "rgb(254, 221, 50)", 1: "rgb(254, 182, 50)" });
   var lightgrad = $(go.Brush, "Linear", { 1: "#E6E6FA", 0: "#FFFAF0" });

   // the template for each attribute in a node's array of item data
   var itemTempl =
     $(go.Panel, "Horizontal",
       $(go.Shape,
         { desiredSize: new go.Size(10, 10) },
         new go.Binding("figure", "figure"),
         new go.Binding("fill", "color")),
       $(go.TextBlock,
         { stroke: "#333333",
           font: "bold 14px sans-serif" },
         new go.Binding("text", "name"))
     );

   // define the Node template, representing an entity
   myDiagram.nodeTemplate =
     $(go.Node, "Auto",  // the whole node panel
       { selectionAdorned: true,
         resizable: true,
         layoutConditions: go.Part.LayoutStandard & ~go.Part.LayoutNodeSized,
         fromSpot: go.Spot.AllSides,
         toSpot: go.Spot.AllSides,
         isShadowed: true,
         shadowColor: "#C5C1AA" },
       new go.Binding("location", "location").makeTwoWay(),
       // whenever the PanelExpanderButton changes the visible property of the "LIST" panel,
       // clear out any desiredSize set by the ResizingTool.
       new go.Binding("desiredSize", "visible", function(v) { return new go.Size(NaN, NaN); }).ofObject("LIST"),
       // define the node's outer shape, which will surround the Table
       $(go.Shape, "Rectangle",
         { fill: lightgrad, stroke: "#756875", strokeWidth: 3 }),
       $(go.Panel, "Table",
         { margin: 8, stretch: go.GraphObject.Fill },
         $(go.RowColumnDefinition, { row: 0, sizing: go.RowColumnDefinition.None }),
         // the table header
         $(go.TextBlock,
           {
             row: 0, alignment: go.Spot.Center,
             margin: new go.Margin(0, 14, 0, 2),  // leave room for Button
             font: "bold 16px sans-serif"
           },
           new go.Binding("text", "key")),
         // the collapse/expand button
         $("PanelExpanderButton", "LIST",  // the name of the element whose visibility this button toggles
           { row: 0, alignment: go.Spot.TopRight }),
         // the list of Panels, each showing an attribute
         $(go.Panel, "Vertical",
           {
             name: "LIST",
             row: 1,
             padding: 3,
             alignment: go.Spot.TopLeft,
             defaultAlignment: go.Spot.Left,
             stretch: go.GraphObject.Horizontal,
             itemTemplate: itemTempl
           },
           new go.Binding("itemArray", "items"))
       )  // end Table Panel
     );  // end Node

   // define the Link template, representing a relationship
   myDiagram.linkTemplate =
     $(go.Link,  // the whole link panel
       {
         selectionAdorned: true,
         layerName: "Foreground",
         reshapable: true,
         routing: go.Link.AvoidsNodes,
         corner: 5,
         curve: go.Link.JumpOver
       },
       $(go.Shape,  // the link shape
         { stroke: "#303B45", strokeWidth: 2.5 }),
       $(go.TextBlock,  // the "from" label
         {
           textAlign: "center",
           font: "bold 14px sans-serif",
           stroke: "#1967B3",
           segmentIndex: 0,
           segmentOffset: new go.Point(NaN, NaN),
           segmentOrientation: go.Link.OrientUpright
         },
         new go.Binding("text", "text")),
       $(go.TextBlock,  // the "to" label
         {
           textAlign: "center",
           font: "bold 14px sans-serif",
           stroke: "#1967B3",
           segmentIndex: -1,
           segmentOffset: new go.Point(NaN, NaN),
           segmentOrientation: go.Link.OrientUpright
         },
         new go.Binding("text", "toText"))
     );
     if(linkModelArray.length == 0){
       myDiagram.model = new go.GraphLinksModel(dataModelArray);
     }else {
       myDiagram.model = new go.GraphLinksModel(dataModelArray, linkModelArray);
     }
 }

// Adapt database to entity relationship model
function adaptDatabaseDataToEnityRelationshipModel(databaseModel){
  var nodeArray = [];
  var objectElement = {};
  var items = [];
  var element = {};
  $.each(databaseModel, function(key, value){
    objectElement['key'] = key;
    $.each(value, function(index, content){
      element['name'] = content;
      if(content == 'id'){
        element['iskey'] = true;
        element['figure'] = "Decision";
        element['color'] = "red";
      } else if(content != 'id' && content.includes("id")){
        element['iskey'] = false;
        element['figure'] = "Cube1";
        element['color'] = "green";
      } else {
        element['iskey'] = false;
        element['figure'] = "TriangleUp";
        element['color'] = "purple";
      }
      items.push(element);
      element = {};
    });
    objectElement['items'] = items;
    nodeArray.push(objectElement);
    objectElement = {};
    items = [];
  });
  return nodeArray;
}

// adapt database RELATIONSHIP to data link model
// We need list tables to remove link model if some tables in RELATIONSHIP
// is not in the list tables, this fix conflit into graphic relationship
function adaptDatabaseRelationshipDataToLinkModel(listTables ,linkModel){
  relationshipModel = [];
  // we return the null object if it is null
  if(linkModel.length == 0){
    linkModel = relationshipModel;
  }else {
    // We have to remove references that match to an non exist tables
    $.each(linkModel, function(key, value){
      $.each(listTables, function(index, content) {
        if(value.REFERENCED_TABLE_NAME == content){
          relationshipModel.push(value);
        }
      });
    });
    // Rename the key in relationship data
    $.each(relationshipModel, function(index, content){
      renameKey('REFERENCED_TABLE_NAME', 'from', content);
      renameKey('TABLE_NAME', 'to', content);
      renameKey('REFERENCED_COLUMN_NAME', 'text', content);
      renameKey('COLUMN_NAME', 'toText', content);
    });
  }
  return relationshipModel;
}

//------------------- This is used to rename key in object ---------------
function renameKey(oldKey, newKey, object){
  object[newKey] = object[oldKey];
  delete object[oldKey];
}
