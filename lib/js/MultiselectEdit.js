//=========================================================================
//  MULTISELECT SEARCH EVENT, INIT SEARCH IN MULTISELECT
//=========================================================================
jQuery(document).ready(function ($) {
    $('#search').multiselect({
        search: {
            left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
            right: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
        },
        fireSearch: function (value) {
            return value.length > 3;
        }
    });
    $("#optgroup").multiselect({
        search: {
            left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
            right: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
        }
    });
    $("#keepRenderingSort").multiselect({
        search: {
            left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
            right: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
        }
    });
    var select = function (context) {
        var ui = $.summernote.ui;
        var button = ui.button({
            contents: 'Select',
            tooltip: 'Select',
            click: function () {
                context.invoke('editor.insertText', 'SELECT');
            }
        });
        return button.render();
    }
    var from = function (context) {
        var ui = $.summernote.ui;
        var button = ui.button({
            contents: 'From',
            tooltip: 'from',
            click: function () {
                context.invoke('editor.insertText', 'FROM');
            }
        });
        return button.render();
    }
    $('#editor').summernote({
        toolbar: [['select', ['select']],
            ['from', ['from']]],
        buttons: {
            select: select,
            from: from
        }
    });
});

//=========================================================================
// INIT SLIDER IN MULTISELECT PAGE
//=========================================================================
var form = $("#example-advanced-form").show();
form.steps({
    headerTag: "h3",
    bodyTag: "fieldset",
    transitionEffect: "slideLeft",
    labels: {
        current: "current step:",
        pagination: "Pagination",
        finish: "Tester",
        next: "Suivant",
        previous: "Revenir",
        loading: "Loading ..."
    },
    onStepChanging: function (event, currentIndex, newIndex) {
        if (newIndex == 1) {
          // get data tab1
          leftDataTab1 = domManager.getTablesInMultiselect('#search');
          rightDataTab1 = domManager.getTablesInMultiselect('#search_to');
          // teste is user make a change
          var ischanged = JSON.stringify(rightDataTab1) != JSON.stringify(tab1.rightData);
          console.log(ischanged);
          if(ischanged){
            tab1['number'] = newIndex;
            tab1['leftData'] = leftDataTab1;
            tab1['rightData'] = rightDataTab1;
            // load data in the tab2
              dataManager.getFieldsOfAllSelectedTables(rightDataTab1, urlGetColumn).done(function(json){
              var leftDataTab2 = dataManager.parseTablesAndFieldsData(json);
              domManager.setTablesAndFielsInMultiselect('#optgroup', leftDataTab2);
            });
          } else {
            domManager.setTablesAndFielsInMultiselect('#optgroup', tab2.leftData);
            domManager.setTablesAndFielsInMultiselect('#optgroup_to', tab2.rightData);
          }
        }
        if (newIndex == 2) {
          // get data tab2
          leftDataTab2 = domManager.getTablesAndFielsInMultiselect('#optgroup');
          rightDataTab2 = domManager.getTablesAndFielsInMultiselect('#optgroup_to');
          // test is user make a change
          var ischanged = JSON.stringify(rightDataTab2) != JSON.stringify(tab2.rightData);
          // Atreto aloha de mbola atao anreto fotsiny mandrapa fa mbola ovaina rehefa veo
          // Manao teste ihany izy fa tsy loadevana loa le data any am joinView
          if(ischanged){
            tab2['number'] = newIndex;
            tab2['leftData'] = leftDataTab2;
            tab2['rightData'] = rightDataTab2;
            joinView.init(tab2.rightData);
          }else {
            joinView.init(tab2.rightData);
          }
          /*
            // Load data in tab3
            leftDataTab3 = rightDataTab2;
            domManager.setTablesAndFielsInMultiselect('#keepRenderingSort', leftDataTab3);
          } else {
            domManager.setTablesAndFielsInMultiselect('#keepRenderingSort', tab3.leftData);
            domManager.setTablesAndFielsInMultiselect('#keepRenderingSort_to', tab3.rightData);
          }
          */
        }

        if(newIndex == 3){
          // get data in join VIEW
          tab3['number'] = newIndex;
          tab3['join_data'] = joinView.getJoinData();
          // append new where layout if needed
          $('.addWhere').on('click', function(){
            whereView.appendNewWhere(tab2.rightData);
          });
        }

        if(newIndex == 4){
          // get data in where VIEW
          tab4['number'] = newIndex;
          tab4['where_data'] = whereView.getWhereData();
          // test is user make a change
          var ischanged = JSON.stringify(rightDataTab2) != JSON.stringify(tab2.rightData);
          // Load data in tab2 for group by view
          leftDataTab5 = rightDataTab2;
          domManager.setTablesAndFielsInMultiselect('#keepRenderingSort', leftDataTab5);
        }

        if (newIndex == 5){
          // show edit text which have the generated request
          $('.requestContent').fadeIn('slow');
          // get data tab3
          leftDataTab5 = domManager.getTablesAndFielsInMultiselect('#keepRenderingSort');
          rightDataTab5 = domManager.getTablesAndFielsInMultiselect('#keepRenderingSort_to');
          tab5['number'] = newIndex;
          tab5['leftData'] = leftDataTab5;
          tab5['rightData'] = rightDataTab5;
          // init relationship graph
          // we need before to adapt database data to datamodel used in entityRelationship graph
          dataManager.getRelationshipTablesOf(tab1.rightData, urlGetTableInForeignKey, function(relation){
                  linkModel = adaptDatabaseRelationshipDataToLinkModel(tab1.rightData, relation);
                  dataModel = adaptDatabaseDataToEnityRelationshipModel(tab2.rightData);
                  // init relationship graph
                  initEntityRelationship(dataModel, linkModel);
                  // Load data to tab4 object
                  tab4['number'] = 6; // this is for number 6 cause linkdata and datamodel are tab4's data
                  tab4['dataModel'] = dataModel;
                  tab4['linkModel'] = linkModel;
            });
        }

        if(newIndex == 5){
          $('#cacher').show();
          var query = getSqlQuery(tab1.rightData, tab2.rightData, tab3.join_data, tab4.where_data, tab5.rightData);
          request['sql_request'] = query;
          $('#visualisation').fadeIn('slow');
          $(".sql").text(query);
        } else {
          $('#cacher').hide();
        }

        // Allways allow previous action even if the current form is not valid!
        if (currentIndex > newIndex) {
            return true;
        }

        // Needed in some cases if the user went back (clean up)
        if (currentIndex < newIndex) {
            // To remove error styles
            form.find(".body:eq(" + newIndex + ") label.error").remove();
            form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
        }
        form.validate().settings.ignore = ":disabled,:hidden";
        return form.valid();
    },
    onStepChanged: function (event, currentIndex, priorIndex) {
        // Used to skip the "Warning" step if the user is old enough.
        if (currentIndex === 2 && Number($("#age-2").val()) >= 18) {
            form.steps("next");
        }
        // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
        if (currentIndex === 2 && priorIndex === 3) {
            form.steps("previous");
        }
    }
    ,
    onFinishing: function (event, currentIndex) {
      /*console.log(sql);*/
      //var requete = getSqlQuery(tab1.rightData, tab2.rightData, tab3.join_data, tab4.where_data, tab5.rightData);
        var requete = $('.sql').val();
        $('#tbl_import').LoadingOverlay("show");
      console.log(requete);
      $.ajax({
          url: urlGetRequests,
          type: 'post',
          data: {
              db: database,
              sql: requete
          },
          success: function (result) {
              console.log(result);
              data = JSON.parse(result);
              selector = '#tbl_import';
              addColumns(data, selector);
              addRows(data, selector);
              $('#tbl_import').LoadingOverlay("hide");
          }
      });
        return true;
    }
    ,
    onFinished: function (event, currentIndex) {
        return true;
    }
}).validate({
    errorPlacement: function errorPlacement(error, element) {
        element.before(error);
    },
    rules: {
        confirm: {
            equalTo: "#password-2"
        }
    }
});
