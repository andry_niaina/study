// Create object requests
//-----------------------
var request = {};
var request_id = '';

//=========================================================================
//  CREATE DATAMANAGER OBJECT
//=========================================================================

// Constructor of the object DataManager
//--------------------------------------
var DataManager =  function(database) {
  this.database = database;
};

// Method get alltables, return ajax object that we can get done method as callback
//---------------------------------------------------------------------------------
DataManager.prototype.getAllTables = function(url) {
  var db_name = this.database;
  return $.ajax({
      type: "POST",
      url: url,
      data: {
          dbselected: db_name
      }
  });
}

// Method get allTables, return ajax object that we can get done method as callback
//---------------------------------------------------------------------------------
DataManager.prototype.getFieldsOfAllSelectedTables = function(allSelectedTables, url){
  var db_name = this.database;
  return $.ajax({
      url: url,
      type: 'POST',
      data: {
          sql: allSelectedTables,
          db: db_name
      }
  });
}

// Method get relationship between several tablesList
//-----------------------------------------------------
DataManager.prototype.getRelationshipTableOf = function(table, url){
  var db_name = this.database;
  return $.ajax({
      url: url,
      type: 'POST',
      data: {
          sql: table,
          bd: db_name
      }
  });
}

// Method get relationship between several tablesList
//----------------------------------------------------
DataManager.prototype.getRelationshipTablesOf = function(tables, url, callback){
  var db_name = this.database;
  var link = [];
  var iter = 1;
  var iterate = $.each(tables, function(key, table){
    $.ajax({
        url: url,
        type: 'POST',
        data: {
            sql: table,
            bd: db_name
        },
        success: function(json){
          data = JSON.parse(json);
          link = link.concat(data);
          iter++;
        }
    });
  });

  setTimeout(function(){
    callback(link);
  }, 1000);
}

//Parse JSON response to get the tablesList data
//-----------------------------------------------
DataManager.prototype.parseTablesData = function (json){
   tablesList = [];
   var data = JSON.parse(json);
   // this is the key that match the table name
   keyData = 'Tables_in_'+this.database;
   $.each(data, function(key, value){
     tablesList.push(value[keyData]);
   });
   return tablesList;
}

// Parse JSON response to get tablesList/fields data
//--------------------------------------------------
DataManager.prototype.parseTablesAndFieldsData = function (json){
   tablesList = {};
   fieldsList = [];
   var data = JSON.parse(json);
   $.each(data, function(key, value){
     $.each(value, function(index, content){
       fieldsList.push(content.COLUMN_NAME);
     });
     tablesList[key] = fieldsList;
     fieldsList = [];
   });
   return tablesList;
}

//=========================================================================
//  CREATE DOMManager OBJECT
//=========================================================================

// Constructor
//  -----------
var DomManager = function(){};

// METHOD GET ALL SELECTED TABLES IN MULTISELECT
//----------------------------------------------
DomManager.prototype.getTablesInMultiselect = function (id){
  var opt = [];
  $(id+">option").each(function(){
    opt.push(this.text);
  });
  return opt;
}

//METHOD GET ALL SELECTED TABLES / FIELD IN MULTISELECT
//------------------------------------------------------
DomManager.prototype.getTablesAndFielsInMultiselect = function (id){
  var optGroup = {};
  var opt = [];
  $(id+">optgroup").each(function(){
    $(this).find('option').each(function(){
        opt.push(this.text);
      });
      optGroup[this.label] = opt;
      opt = [];
  });
  return optGroup;
}

// METHOD SET TABLES LIST IN MULTISELECT
//--------------------------------------
DomManager.prototype.setTablesInMultiselect = function (id, data) {
  var opt = [];
  $.each(data, function(key, value){
    opt.push('<option value="' + value + '">' + value + '</option>');
  });
  $(id).html(opt.join(' '));
}

// METHOD SET TABLES/FIELS IN MULTISELECT
//----------------------------------------
DomManager.prototype.setTablesAndFielsInMultiselect = function(id, data){
  var optgroup = [];
  $.each(data, function(key, value){
    optgroup.push('<optgroup label="' + key + '">');
    $.each(value, function(index, content){
      optgroup.push('<option value="' + content + '">' + content + '</option>');
    });
    optgroup.push('</optgroup>');
  });
  $(id).html(optgroup.join(' '));
}


//=========================================================================
//  EXCHANGING INFORMATION TO MULTISELECT VIEW
//=========================================================================

// create Manager object
var dataManager = null; // init after database selected
var domManager = new DomManager();

//=========================================================================
//              GENERATE SQL QUERY FROM SELECTED DATA
//=========================================================================
function getSqlQuery(selectedTables, selectedFields, join, where, orderBy){
    var request = [];
    request.push('SELECT \n');
    // get fields list
    $.each(selectedFields, function(key, value){
      $.each(value, function(index, content){
          request.push(key+"."+content);
          request.push(", ");
      });
    });
    // remove the last ", "
    request.pop();
    // insert reference table from join
    if(join.length != 0){
      console.log(join);
      var refTable = join[0].REF_TABLE;
      request.push('\nFROM '+refTable+'\n');
      // insert join relation
      $.each(join, function(key, value){
        request.push(value.JOIN_TYPE+" "+value.TABLE+" ON "+value.REF_COLUMN+" = "+value.COLUMN);
        request.push("\n");
      });
        request.pop();
    }else{
      request.push('\nFROM ');
      $.each(selectedTables, function(key, value){
        request.push(value);
        request.push(", ");
      });
      request.pop();
    }

    if(where.length != 0){
      // insert where clause
      request.push("\nWHERE ");
      $.each(where, function(key, value){
        if(value.operator == "EGAL"){
          request.push(value.field+" = "+value.value);
        }else if (value.operator == "DIFFERENT") {
          request.push(value.field+" <> "+value.value);
        }else if (value.operator == "ENTRE") {
          request.push(value.field+" BETWEEN "+value.value);
        }else if (value.operator == "CONTIENT") {
          request.push(value.field+" LIKE '%"+value.value+"%'");
        }else {
          request.push(value.field+" "+value.operator+" "+value.value);
        }
        request.push(" AND ");
      });
      // remove the last AND
      request.pop();
      request.push("\n");
    }
    if(orderBy != null){
      // get allGroupBy list
      request.push("\nORDER BY ");
      $.each(orderBy, function(key, value){
        $.each(value, function(index, content){
            request.push(key+"."+content);
            request.push(", ");
        });
      });
      // remove the last ", "
      request.pop();
    }
    request.push(';');
    return request.join("");
}

//=========================================================================
//              GENERATE CREATED TABLES TO STORE REQUEST RESPONSE
//=========================================================================
function addColumns(jsonObj, selector) {
    var theadHtml = '';
    var c=0; // Just temp variable
    $.each(jsonObj, function (index, value) {
        if (index == 0) {
            for (var property in value) {
                if (value.hasOwnProperty(property))
                    theadHtml += '<th>' + property + '</th>';
                c++;
            }
        }
        $(selector + ' thead tr').html(theadHtml);
    });
}

function addRows(jsonObj, selector) {
    var tbody = $(selector + ' tbody');
    $.each(jsonObj, function (i, d) {
        var rowTr = '<tr>';
        var rowTrf ='</tr>';
        rw = rowToappend(d);
        row = rowTr+rw.join('')+rowTrf;
        tbody.append(row);
    });
}

function rowToappend(d){
    var rw = [];
    $.each(d, function (j, e) {
        rw.push('<td>' + e + '</td>');
    });
    return rw;
}

//=========================================================================
//              WHERE OBJECT
//=========================================================================
function WhereView(){
  //--------------------------- Attribut -------------------------------------
  this.table = $('.whereTables');
  this.whereData = [];
  //--------------------------- Append new Where Method -----------------------
  this.appendNewWhere = function(tablesAndFields){
    var tablesData = [];
    tablesData.push('<tr><td>');
    tablesData.push('<select class="whereField" name="">');
    $.each(tablesAndFields, function(key, value){
      tablesData.push('<optgroup label="'+key+'">');
      $.each(value, function(index, content){
        tablesData.push('<option value="'+key+"."+content+'"><span style="color: red;">'+key+'</span>.'+content+'</option>');
      });
      tablesData.push('</optgroup>');
    });
    tablesData.push('</select>');
    tablesData.push('</td>');
    tablesData.push('<td>');
    tablesData.push('<select class="whereOp" name="">');
      tablesData.push('<option value="EGAL">EGAL</option>');
      tablesData.push('<option value="DIFFERENT">DIFFERENT</option>');
      tablesData.push('<option value=">"> > </option>');
      tablesData.push('<option value="<"> < </option>');
      tablesData.push('<option value="<="> <= </option>');
      tablesData.push('<option value=">="> >= </option>');
      tablesData.push('<option value="ENTRE"> ENTRE </option>');
      tablesData.push('<option value="CONTIENT"> CONTIENT </option>');
    tablesData.push('</select>');
    tablesData.push('</td>');
    tablesData.push('<td><input type="text" name="" value=""></td>');
    tablesData.push('</tr>');
    this.table.find('.whereTBody').append(tablesData.join(''));
  }
  //-------------------------------- Get where data method ---------------------------
  this.getWhereData = function(){
    var fields = [];
    var op = [];
    var val = [];
    this.table.find('.whereField').find(':selected').each(function(){
      fields.push(this.value);
    });
    this.table.find('.whereOp').find(':selected').each(function(){
      op.push(this.value);
    });
    this.table.find('input').each(function(){
      val.push(this.value);
    });
    for (var i = 0; i < fields.length; i++) {
      this.whereData.push({
        "field": fields[i],
        "operator": op[i],
        "value": val[i]
      });
    }
    return this.whereData;
  }
}

whereView = new WhereView();
//=========================================================================
//              JOIN OBJECT
//=========================================================================
function JoinView(){
  this.dataJoin = {};
  this.ref_table = {};
  this.nbClick = 0;
//------------------INIT----------------------
  this.init = function(tablesAndFields){
    //init data Join
    this.dataJoin = jQuery.extend(true, {}, tablesAndFields);
    // load tables list
    $.each(this.dataJoin, function(key, value){
      $('.refTable').append('<option value="'+key+'">'+key+'</option>');
    });
    self = this;
    // when reference table selected
    $('.addJoin').on('click',function(){
      if(self.nbClick != 0){
        var i = self.nbClick - 1;
        var t = $("#tr"+i).find('.selectTable').find(':selected').text();
        if(self.dataJoin[t] != undefined){
          delete self.dataJoin[t];
        }
      }
      var ref_table_selected = $(".refTable").find(':selected').text();
      if(self.dataJoin[ref_table_selected] != undefined){
        self.ref_table[ref_table_selected] = self.dataJoin[ref_table_selected];
        delete self.dataJoin[ref_table_selected];
      }
      self.generateJoinEditor();
      // when join editor is generate, we need to execute again this.event
      self.event();
      // Increment number click when we create new join view
      self.nbClick++;
    });
    this.event();
  }
//--------------------EVENT--------------------
  this.event = function(){
    self = this;
    // when table is selected, we need only the matched fiels in this tables
    $('.selectTable').on('change', function(){
      var table = $(this).find(':selected').text();
      var col = $(this).parents('tr').find('.selectColumn');
      var htmlContentForThis = [];
      htmlContentForThis.push('<optgroup label="'+table+'">');
      $.each(self.dataJoin[table], function(index, content){
        htmlContentForThis.push('<option value="'+table+"."+content+'">'+table+'.'+content+'</option>');
      });
      htmlContentForThis.push('</optgroup>');
      col.html(htmlContentForThis.join(''));
    });
  }
//--------------------GenerateJoinEditor--------------------
  this.generateJoinEditor = function(){
    var htmlContent = [];
    var joinOp = ['INNER JOIN','LEFT JOIN', 'RIGHT JOIN', 'FULL JOIN'];
    htmlContent.push('<tr id="tr'+this.nbClick+'">blabla <td>');
    //select join option
    htmlContent.push('<select>');
    $.each(joinOp, function(key, val){
      htmlContent.push('<option value="'+val+'">'+val+'</option>');
    });
    htmlContent.push('</select></td>');
    // select table
    htmlContent.push('<td><select class="selectTable">');
    $.each(this.dataJoin, function(key, value){
      htmlContent.push('<option value="'+key+'">'+key+'</option>');
    });
    htmlContent.push('<select></td>');
    // ON operator
    htmlContent.push('<td>ON</td>');
    // select reference field
    var refField = $('.refTable').find(':selected').text();
    htmlContent.push('<td><select>');
    $.each(this.ref_table[refField], function(key, val){
      htmlContent.push('<option value="'+refField+"."+val+'">'+refField+"."+val+'</option>');
    });
    htmlContent.push('</td></select>');
    // select field
    htmlContent.push('<td><select class="selectColumn">');
    $.each(this.dataJoin, function(key, value){
      htmlContent.push('<optgroup label="'+key+'">');
      $.each(value, function(index, content){
        htmlContent.push('<option value="'+key+"."+content+'"><span style="color: red;">'+key+'</span>.'+content+'</option>');
      });
      return false;
      htmlContent.push('</optgroup>');
    });
    htmlContent.push('</select></td>');
    htmlContent.push('</tr>');
    $('.joinLayout').append(htmlContent.join(' '));
  }

  //--------------------------GetJoinData------------------------------
  this.getJoinData = function(){
    var joinData = [];
    var items = {};
    var ref_table = $('.refTable').find(':selected').text();
    var tableRow = $('.joinLayout').find('tr');
    tableRow.each(function(){
      items['REF_TABLE'] = ref_table;
      $(this).find(':selected').each(function(key, value){
        if(key == 0){
          items['JOIN_TYPE'] = $(this).text();
        }else if (key == 1) {
          items['TABLE'] = $(this).text();
        }else if (key == 2) {
          items['REF_COLUMN'] = $(this).text();
        }else if (key == 3) {
          items['COLUMN'] = $(this).text();
        }
      });
      joinData.push(items);
      items = {};
    });
    return joinData;
  }
}
var joinView = new JoinView();
