  //=========================================================================
// CALL BACK USED TO APPEND OPTION VALUES TABLES LIST METADATA
//=========================================================================
// load tables list and put it to the multipleselect
function appendOption(response, selector, id) {
    var list = [];
    $.each(response, function (key, val) {
        list.push('<option value="' + val[id] + '">' + val[id] + '</option>');
    });
    selector.html(list.join(''));
}

//=========================================================================
// INIT GO JS FOR ENTITY RELATIONSHIP DIAGRAM
//=========================================================================
// this is an array used to create mode of the entity RELATIONSHIP graph
var dataModelArray = [];

function initEntityRelationship() {
    if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
    var $ = go.GraphObject.make;  // for conciseness in defining templates

    myDiagram =
        $(go.Diagram, "myDiagramDiv",  // must name or refer to the DIV HTML element
            {
                initialContentAlignment: go.Spot.Center,
                allowDelete: false,
                allowCopy: false,
                layout: $(go.ForceDirectedLayout),
                "undoManager.isEnabled": true
            });

    // define several shared Brushes
    var bluegrad = $(go.Brush, "Linear", {0: "rgb(150, 150, 250)", 0.5: "rgb(86, 86, 186)", 1: "rgb(86, 86, 186)"});
    var greengrad = $(go.Brush, "Linear", {0: "rgb(158, 209, 159)", 1: "rgb(67, 101, 56)"});
    var redgrad = $(go.Brush, "Linear", {0: "rgb(206, 106, 100)", 1: "rgb(180, 56, 50)"});
    var yellowgrad = $(go.Brush, "Linear", {0: "rgb(254, 221, 50)", 1: "rgb(254, 182, 50)"});
    var lightgrad = $(go.Brush, "Linear", {1: "#E6E6FA", 0: "#FFFAF0"});

    // the template for each attribute in a node's array of item data
    var itemTempl =
        $(go.Panel, "Horizontal",
            $(go.Shape,
                {desiredSize: new go.Size(10, 10)},
                new go.Binding("figure", "figure"),
                new go.Binding("fill", "color")),
            $(go.TextBlock,
                {
                    stroke: "#333333",
                    font: "bold 14px sans-serif"
                },
                new go.Binding("text", "name"))
        );

    // define the Node template, representing an entity
    myDiagram.nodeTemplate =
        $(go.Node, "Auto",  // the whole node panel
            {
                selectionAdorned: true,
                resizable: true,
                layoutConditions: go.Part.LayoutStandard & ~go.Part.LayoutNodeSized,
                fromSpot: go.Spot.AllSides,
                toSpot: go.Spot.AllSides,
                isShadowed: true,
                shadowColor: "#C5C1AA"
            },
            new go.Binding("location", "location").makeTwoWay(),
            // whenever the PanelExpanderButton changes the visible property of the "LIST" panel,
            // clear out any desiredSize set by the ResizingTool.
            new go.Binding("desiredSize", "visible", function (v) {
                return new go.Size(NaN, NaN);
            }).ofObject("LIST"),
            // define the node's outer shape, which will surround the Table
            $(go.Shape, "Rectangle",
                {fill: lightgrad, stroke: "#756875", strokeWidth: 3}),
            $(go.Panel, "Table",
                {margin: 8, stretch: go.GraphObject.Fill},
                $(go.RowColumnDefinition, {row: 0, sizing: go.RowColumnDefinition.None}),
                // the table header
                $(go.TextBlock,
                    {
                        row: 0, alignment: go.Spot.Center,
                        margin: new go.Margin(0, 14, 0, 2),  // leave room for Button
                        font: "bold 16px sans-serif"
                    },
                    new go.Binding("text", "key")),
                // the collapse/expand button
                $("PanelExpanderButton", "LIST",  // the name of the element whose visibility this button toggles
                    {row: 0, alignment: go.Spot.TopRight}),
                // the list of Panels, each showing an attribute
                $(go.Panel, "Vertical",
                    {
                        name: "LIST",
                        row: 1,
                        padding: 3,
                        alignment: go.Spot.TopLeft,
                        defaultAlignment: go.Spot.Left,
                        stretch: go.GraphObject.Horizontal,
                        itemTemplate: itemTempl
                    },
                    new go.Binding("itemArray", "items"))
            )  // end Table Panel
        );  // end Node

    // define the Link template, representing a relationship
    myDiagram.linkTemplate =
        $(go.Link,  // the whole link panel
            {
                selectionAdorned: true,
                layerName: "Foreground",
                reshapable: true,
                routing: go.Link.AvoidsNodes,
                corner: 5,
                curve: go.Link.JumpOver
            },
            $(go.Shape,  // the link shape
                {stroke: "#303B45", strokeWidth: 2.5}),
            $(go.TextBlock,  // the "from" label
                {
                    textAlign: "center",
                    font: "bold 14px sans-serif",
                    stroke: "#1967B3",
                    segmentIndex: 0,
                    segmentOffset: new go.Point(NaN, NaN),
                    segmentOrientation: go.Link.OrientUpright
                },
                new go.Binding("text", "text")),
            $(go.TextBlock,  // the "to" label
                {
                    textAlign: "center",
                    font: "bold 14px sans-serif",
                    stroke: "#1967B3",
                    segmentIndex: -1,
                    segmentOffset: new go.Point(NaN, NaN),
                    segmentOrientation: go.Link.OrientUpright
                },
                new go.Binding("text", "toText"))
        );
//---------------------------------------------
    // create the model for the E-R diagram
//---------------------------------------------
  var link = [];
  for (var i = 0; i < linkDataArray.length; i++) {
    if(linkDataArray[i].length != 0){
      link = linkDataArray[i];
    }
  }
   myDiagram.model = new go.GraphLinksModel(dataModelArray, link);
   myDiagram.setSize (100, 100);
   myDigaram.redrawe();
 }


//=========================================================================
// CALL BACK METHOD BUTTON NEXT
//=========================================================================
//$('.bootgrid-table td.loading, .bootgrid-table td.no-results').remove();
$('#visualisation').hide();
$('#tbl_import').bootstrapTable();
var form = $("#example-advanced-form").show();
form.steps({
    headerTag: "h3",
    bodyTag: "fieldset",
    transitionEffect: "slideLeft",
    labels: {
        current: "current step:",
        pagination: "Pagination",
        finish: "Tester",
        next: "Suivant",
        previous: "Revenir",
        loading: "Loading ..."
    },
    onStepChanging: function (event, currentIndex, newIndex) {
        var allSelectedTables = [];
        $('#search_to option').each(function () {
            allSelectedTables.push($(this).val());
        });

        /*select all table and its fields*/
        if (newIndex == 1) {
            // Get all selections tables
            var db_name = $('.status').text().toString();
            // Send/receive info on the table via ajax-post
            $.ajax({
                url: urlGetColumn,
                type: 'post',
                data: {
                    sql: allSelectedTables,
                    db: db_name
                },
                success: function (result) {
                    optGroup = $('select[name="from"]');
                    data = JSON.parse(result);
                    $.each(data, function (key, group) {
                        var label = [];
                        var option = [];
                        label.push('<optgroup value="' + key + '" label="' + key + '">');
                        $.each(group, function (ukey, opt) {
                            option.push('<option value="' + opt['COLUMN_NAME'] + '">' + opt['COLUMN_NAME'] + '</option>');
                        });
                        label.push(option);
                        label.push('</optgroup>');
                        optGroup.append(label.join(''));
                    })
                }
            });
        }

        if (newIndex == 1) {
            var fk = [];
            tables = getSelectedTables();
            for (var i = 0; i < tables.length; i++) {
                console.log('relation');
                getRelationship(tables[i]);
            }
        }

        if (newIndex == 2) {
            var selector = $('select#keepRenderingSort');
// -------------- ARRAY AND OBJECTS TO CREATE MODEL ENTITY RELATIONSHIP GRAPH ----------
            var itemArray = [];
            var dataModelObject = {};
//-------------------------------------------------------------------------------------
            $('select#optgroup_to optgroup').each(function () {
                dataModelObject["key"] = this.label; // Add first key to the data object
                var optGroup = [];
                var option = [];
                var optionJoined = '';
                //console.log(this.label);
                optGroup.push('<optgroup value="' + this.label + '" label="' + this.label + '">');
                $(this).find('option').each(function () {
                  // create item array to insert as 2nd content of the dataModelObject
                  fieldSelectedList.push($(this).val());
                  if($(this).val() == 'id'){
                    itemArray.push({"name": $(this).val(), iskey: true, figure: "Decision", color: 'red'});
                  } else if($(this).val() != 'id' && $(this).val().includes("id")){
                    itemArray.push({"name": $(this).val(), iskey: true, figure: "Cube1", color: 'green'});
                  } else {
                    itemArray.push({"name": $(this).val(), iskey: true, figure: "TriangleUp", color: 'purple'});
                  }
                  // create html tag for multiselect view
                    option.push('<option value="' + $(this).val() + '">' + $(this).val() + '</option>');
                });
                // insert itemArray into dataModelObject as 2nd content
                dataModelObject["items"] = itemArray;
                // then insert the complete dataModelArray in the dataModelArray to create relationship model
                dataModelArray.push(dataModelObject);
                // clear dataModelArray and itemArray to store another data in the loop
                dataModelObject = {};
                itemArray = [];
                // this is table to generate html element for multiselect
                optionJoined += option.join('');
                optGroup.push(optionJoined);
                optGroup.push('</optgroup>');
                //console.log(optGroup.join(''));
                selector.append(optGroup.join(''));
            });
            adaptEntityRationData();
        }
        if (newIndex == 3) {
            if (!$("#myDiagramDiv").html() == "") {
                myDiagram.rebuildParts
            } else {
                initEntityRelationship();
            }
        }
        if (newIndex == 3) {
            $('#visualisation').show();
            var query = getSqlQuery();
            //console.log(query);
           $("#editor").summernote('code', query);
        } else {
            $('#visualisation').hide();
        }
        // Allways allow previous action even if the current form is not valid!
        if (currentIndex > newIndex) {
            return true;
        }

        // Needed in some cases if the user went back (clean up)
        if (currentIndex < newIndex) {
            // To remove error styles
            form.find(".body:eq(" + newIndex + ") label.error").remove();
            form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
        }
        form.validate().settings.ignore = ":disabled,:hidden";
        return form.valid();
    },
    onStepChanged: function (event, currentIndex, priorIndex) {
        // Used to skip the "Warning" step if the user is old enough.
        if (currentIndex === 2 && Number($("#age-2").val()) >= 18) {
            form.steps("next");
        }
        // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
        if (currentIndex === 2 && priorIndex === 3) {
            form.steps("previous");
        }
    }
    ,
    onFinishing: function (event, currentIndex) {
        /*console.log(sql);*/
        database = $('.status').text();
        console.log(database);
        var requete = getSqlQuery();
        $.ajax({
            url: urlGetRequests,
            type: 'post',
            data: {
                db: database,
                sql: requete
            },
            success: function (result) {
                //console.log(result);
                data = JSON.parse(result);
                selector = '#tbl_import';
                addColumns(data, selector);
                addRows(data, selector);
            }
        });
        return true;
    }
    ,
    onFinished: function (event, currentIndex) {

    }
}).validate({
    errorPlacement: function errorPlacement(error, element) {
        element.before(error);
    },
    rules: {
        confirm: {
            equalTo: "#password-2"
        }
    }
});

/*iteration of select field*/
function iteratResultField(selector) {
    var column = [];
    var table = [];
    var requete = [];
    selector.each(function () {
        table.push(this.label);
        tab = this.label;
        $(this).find('option').each(function () {
            requete.push(tab + '.' + $(this).val());
        });
        reqString = requete.join(',');
        column.push(reqString);
    });
    return column;
}

/*get column name to display */
function getSqlQuery() {
    var req = [];
    var link = [];
    var column = [];
    var table = [];
    var selectColumn = $('select#optgroup_to optgroup');
    var selectGroupBy = $('select#keepRenderingSort_to optgroup');
    var allselection = iteratResultField(selectColumn);
    var allGroupBy = iteratResultField(selectGroupBy);
    for (var i = 0; i < allselection.length; i++) {
        if (i == 0) {
            column[i] = allselection[i];
        } else {
            column[i] = allselection[i].substring(allselection[i].indexOf(column[i - 1]) + column[i - 1].length + 1);
        }
        table[i] = column[i].substring(0, column[i].indexOf('.'));
    }
    for (var i = 0; i < linkDataArray.length; i++) {
        if (linkDataArray[i].length != 0) {
            link = linkDataArray[i];
        }
    }
    var tableref;
    console.log(link);
    if(link.length>0 && table.length>1){
        for(var k=0;k<link.length;k++){
            for(var t=0;t<table.length;t++){
                if(link[k]['to']==table[t]){
                    tableref = table[t]
                    req.push('join '+link[k]['from']+' on '+table[t]+'.'+link[k]['toText']+'='+link[k]['from']+'.'+link[k]['text']);
                }
            }
        }
        var sqlText = req.join(' ');
        return requete = 'SELECT '+ column.join(',') + ' FROM ' + tableref +' '+sqlText+' ORDER BY ' + allGroupBy.pop() + ';';
        console.log(tableref);
        console.log(req);

    }else{
        return requete = 'SELECT ' + column.join(',') + ' FROM ' + table.join(',') + ' ORDER BY ' + allGroupBy.pop() + ';';
        console.log(requete);
    }

}

function addColumns(jsonObj, selector) {
    var theadHtml = '';
    var c=0;
    $.each(jsonObj, function (index, value) {
        if (index == 0) {
            for (var property in value) {
                if (value.hasOwnProperty(property))
                    theadHtml += '<th>' + property + '</th>';
                c++;
            }
        }
        $(selector + ' thead tr').html(theadHtml);
    });
}

function addRows(jsonObj, selector) {
    var tbody = $(selector + ' tbody');
    $.each(jsonObj, function (i, d) {
        var rowTr = '<tr>';
        var rowTrf ='</tr>';
        rw = rowToappend(d);
        row = rowTr+rw.join('')+rowTrf;
        tbody.append(row);
    });
}

//=========================================================================
// ADAPT RELATION OBJET TO ENTITY RELATIONSHIP MODEL OBJECT
//=========================================================================
function adaptEntityRationData(){
  for (var i = 0; i < linkDataArray.length; i++) {
    for (var j = 0; j < linkDataArray[i].length; j++) {
      renameKey('REFERENCED_TABLE_NAME', 'from', linkDataArray[i][j]);
      renameKey('TABLE_NAME', 'to', linkDataArray[i][j]);
      renameKey('REFERENCED_COLUMN_NAME', 'text', linkDataArray[i][j]);
      renameKey('COLUMN_NAME', 'toText', linkDataArray[i][j]);
    }
}
//------------------- This is used to rename key in array ---------------
function renameKey(oldKey, newKey, object){
  object[newKey] = object[oldKey];
  delete object[oldKey];
}

//=========================================================================
// CALL MODAL AND INSERT SQL INTO TEXTAREA
//=========================================================================
function loadRequest(){
  $('.requestTextArea').text(getSqlQuery());
}
//=========================================================================
// EVENT SAVE BUTTON MODAL AND INSERT DATA IN DATABASE
//=========================================================================
var nameRequest = "";
var descriptionRequest = "";
function getFormContent(form){
  nameRequest = form.name.value;
  descriptionRequest = form.descriptionRequest.value;
  $('.modal-dialog').LoadingOverlay("show");
  $.ajax({
    url: urlInsertRequest,
    type: 'post',
    data:{
      name: nameRequest,
      description: descriptionRequest,
      sql_request: getSqlQuery()
    },
    success: function(result){
      if(result != null){
        //insertField(result);
        document.location.href = urlRequestsList;
      }else{
        alert("Une erreur s'est produite");
      }
    }
  });
}

function insertField(requestId) {
  var fields = JSON.stringify(fieldSelectedList);
  // console.log(fields);
  $.ajax({
    url: urlInsertRequest,
    type: 'post',
    data:{
      requestId: requestId,
      fields: fields
    },
    success: function(result){
      console.log(result);
    }
  });
  // console.log(fieldSelectedList);
}
}

function rowToappend(d){
    var rw = [];
    $.each(d, function (j, e) {
        rw.push('<td>' + e + '</td>');
    });
    return rw;
}
