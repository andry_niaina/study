//=========================================================================
//  MULTISELECT SEARCH EVENT
//=========================================================================
jQuery(document).ready(function ($) {
    $('#search').multiselect({
        search: {
            left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
            right: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
        },
        fireSearch: function (value) {
            return value.length > 3;
        }
    });
    $("#optgroup").multiselect({
        search: {
            left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
            right: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
        }
    });
    $("#keepRenderingSort").multiselect({
        search: {
            left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
            right: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
        }
    });
    var select = function (context) {
        var ui = $.summernote.ui;
        var button = ui.button({
            contents: 'Select',
            tooltip: 'Select',
            click: function () {
                context.invoke('editor.insertText', 'SELECT');
            }
        });
        return button.render();
    }
    var from = function (context) {
        var ui = $.summernote.ui;
        var button = ui.button({
            contents: 'From',
            tooltip: 'from',
            click: function () {
                context.invoke('editor.insertText', 'FROM');
            }
        });
        return button.render();
    }
    $('#editor').summernote({
        toolbar: [['select', ['select']],
            ['from', ['from']]],
        buttons: {
            select: select,
            from: from
        }
    });
//============================================================================
    // EVENT SELECTION BASE DE DONNEE ET CHARGEMENT DES TABLES VIA JSON
//============================================================================
    $('.status').on('click', function () {
        //alert('ok');
        var db_name = $('.status').text();
        $.ajax({
            type: "POST",
            url: urlAjaxAllTable,
            data: {
                dbselected: db_name
            },
            success: function (response) {
                id = "Tables_in_" + db_name;
                var select = $('select[class~="tables"]');
                data = JSON.parse(response);
                appendOption(data, select, id);
            }
        });
    });
});

//=============================================================================
// GET COLUMN METADATA
//=============================================================================
$('a[href="#next"]').on('click', function () {
    // get tables selected in multiselect
    var allSelectedTables = [];
    $('#search_to option').each(function () {
        allSelectedTables.push($(this).val());
    });
});
//=============================================================================
// GET SELECTED TABLES
//=============================================================================
function getSelectedTables(){
  allTables = [];
  $('#search_to option').each(function () {
      allTables.push($(this).val());
  });
  return allTables;
}

//=============================================================================
// GET TABLES RELATIONSHIP
//=============================================================================
function getRelationship(table){
  var relationship = [];
  $.ajax({
      url: urlGetTableInForeignKey,
      type: 'post',
      data: {
          sql: table,
          bd: $('.status').text()
      },
      success: function (result) {
          d = JSON.parse(result);
          Object.keys(d).map(function(k) {
              relationship.push(d[k]);
          });
          /*for(var i = 0; i < d.length ; i++){
            relationship.push(d[i]);
          }*/
          //console.log('relation');
          //console.log(relationship.length);
          linkDataArray.push(relationship);
      }
  });
    return relationship;
  }
