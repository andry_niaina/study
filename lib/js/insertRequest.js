jQuery(document).ready(function($){
  $('.requestContent').hide();
  $('.panel').hide();
});
// STEP 1: Load all tables after clicking button status
$('.status').on('click', function(){
  // Load table when database is selected
  database = $(this).text();
  dataManager = new DataManager(database);
  dataManager.getAllTables(urlAjaxAllTable).done(function(json){
    tables = dataManager.parseTablesData(json);
    domManager.setTablesInMultiselect('#search', tables);
  });
});
//=========================================================================
// CALL MODAL AND INSERT SQL INTO TEXTAREA
//=========================================================================
function loadRequest(){
  $('.requestTextArea').text(request.sql_request);
}

//=========================================================================
// SEND SQL DATA TO SERVER
//=========================================================================
// we need to send request first to get last request id after
function getFormContent(form){
  var nameRequest = form.name.value;
  var descriptionRequest = form.descriptionRequest.value;
  $('.modal-dialog').LoadingOverlay("show");
  $.ajax({
    url: urlInsertRequest,
    type: 'post',
    data:{
      name: nameRequest,
      description: descriptionRequest,
      sql_request: request.sql_request
    },
    success: function(result){
      document.location.href = urlRequestsList;
    }
  });
}

// when we send request in server, we need the last insert id before
// so we need to send all fields first
// so we can insert all fields that match the request
function insertField(form) {
  var fields = [];
  var lastRequestId = '';
  $.each(tab2.rightData, function(key, value){
    $.each(value, function(index, content){
      fields.push(key+'.'+content);
    });
  });
  // get last request id
  $.ajax({
    url: urlLastRequestId,
    type: "post",
    success: function(id){
      id++;
      request_id = id;
      $.ajax({
        url: urlInsertField,
        type: 'post',
        data:{
          requestId: id,
          fields: fields
        },
        success: function(log){
            sendConfig();
            getFormContent(form);
        }
      });
    }
  });
}

// if all things is done, we can send config data to simplify edit fonctionality
function sendConfig(){
  console.log(tab5);
  console.log(tab6);
  $.ajax({
    url: urlConfigEditor,
    type: "post",
    data:{
      db: database,
      tab1: JSON.stringify(tab1),
      tab2: JSON.stringify(tab2),
      tab3: JSON.stringify(tab3),
      tab4: JSON.stringify(tab4),
      tab5: JSON.stringify(tab5),
      tab6: JSON.stringify(tab6),
      request_id: request_id
    },
    success: function(result){
      //document.location.href = urlRequestsList;
    }
  });
}
