// STEP 1: Load all tables automatically
jQuery(document).ready(function($){
  // Load table when database is selected
  console.log(database);
  $('.status').text(database);
  dataManager = new DataManager(database);
  dataManager.getAllTables(urlAjaxAllTable).done(function(json){
  tables = dataManager.parseTablesData(json);
  domManager.setTablesInMultiselect('#search', tables);
  // Load tab1 left data
  domManager.setTablesInMultiselect('#search_to', tab1.rightData);
  $('.requestContent').hide();
  $('#visualisation').hide();
});
});


// We need to delete old fields in database and change new one
function deleteOldFields(form){
  $.ajax({
    url: urlDeletOldFields,
    type: "post",
    data: {
      id: requestId
    },
    success: function(result){
      insertField(form);
    }
  });
}

// so we need to send all fields first
// so we can insert all fields that match the request
function insertField(form) {
  var fields = [];
  $.each(tab2.rightData, function(key, value){
    $.each(value, function(index, content){
      fields.push(content);
    });
  });
  $.ajax({
    url: urlInsertField,
    type: 'post',
    data:{
      requestId: requestId,
      fields: fields
    },
    success: function(result){
      editRequest(form);
    }
  });
  }

//we need to get request data to edit it
function getRequest(){
  $.ajax({
    url: urlGetRequestById,
    type: 'post',
    data:{
      id: requestId
    },
    success: function(result){
      req = JSON.parse(result);
      loadModal(req);
    }
  });
}

// edit request
function editRequest(form){
  var nameRequest = form.name.value;
  var descriptionRequest = form.descriptionRequest.value;
  var sqlRequest = $('.requestTextArea').val();
  $('.modal-dialog').LoadingOverlay("show");
  $.ajax({
    url: urlEditRequest,
    type: "post",
    data:{
      id: requestId,
      name: nameRequest,
      description: descriptionRequest,
      sql_request: sqlRequest
    },
    success: function(result){
      sendConfig();
    }
  });
}
// we can now edit request and load modal
function loadModal(req){
  $('#nameRequest').val(req.name);
  $('#request').val(req.description);
  $('.requestTextArea').val(request.sql_request);
}

// if all things is done, we can send config data to simplify edit fonctionality
function sendConfig(){
  $.ajax({
    url: urlUpdateConfigEditor,
    type: "post",
    data:{
        db: database,
      tab1: JSON.stringify(tab1),
      tab2: JSON.stringify(tab2),
      tab3: JSON.stringify(tab3),
      tab4: JSON.stringify(tab4),
      tab5: JSON.stringify(tab5),
      tab6: JSON.stringify(tab6),
      request_id: request_id
    },
    success: function(result){
      document.location.href = urlRequestsList;
    }
  });
}
