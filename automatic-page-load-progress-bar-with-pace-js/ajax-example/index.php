<!doctype html>
<html>
    <head>
        <title>Check username availability with jQuery and AJAX</title>
        <script src="jquery-3.1.1.min.js" type="text/javascript"></script>
        
        <link href='style.css' rel='stylesheet' type='text/css'>
        <link href="../pace-master/themes/blue/pace-theme-center-circle.css" rel="stylesheet" />
        <script src='../pace-master/pace.min.js' type='text/javascript'></script>

        <script>
            $(document).ready(function(){

                $("#txt_uname").change(function(){

                    var uname = $("#txt_uname").val().trim();

                    if(uname != ''){

                        $("#uname_response").show();
                        
                        $.ajax({
                            url: 'uname_check.php',
                            type: 'post',
                            data: {uname:uname},
                            success: function(response){
                                
                                // Show status
                                if(response > 0){
                                    $("#uname_response").html("<span class='not-exists'>* Username Already in use.</span>");

                                }else{
                                    $("#uname_response").html("<span class='exists'>Available.</span>");

                                }

                            }
                        });
                    }else{
                        $("#uname_response").hide();
                    }

                });

                // Pace.js
                $(document).ajaxStart(function() { 
                    Pace.restart(); 
                });
            });
        </script>


    </head>
    <body>

        <div class="container">

            <div id="div_reg">
                <h1>Registration</h1>

                <div>
                    <input type="text" class="textbox" id="txt_name" name="txt_name" placeholder="Name"/>
                </div>

                <!-- Username -->
                <div>
                    <input type="text" class="textbox" id="txt_uname" name="txt_uname" placeholder="Username"  />
                    <div id="uname_response" class="response"></div>
                </div>

                <div>
                    <input type="password" class="textbox" id="txt_pwd" name="txt_pwd" placeholder="Password"/>
                </div>

                <div>
                    <input type="password" class="textbox" id="txt_repwd" name="txt_repwd" placeholder="Re-type Password"/>
                </div>

                <div>
                    <input type="submit" value="Submit" name="but_submit" id="but_submit" />
                </div>

            </div>

        </div>

    </body>
</html>